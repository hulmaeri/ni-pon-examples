from functools import reduce
import matplotlib.pyplot as plt
import numpy as np

DBL_EPS = 1e-9


def bool_index_to_xy(bool_indexing):
    n = bool_indexing.shape[0]
    x, y = np.meshgrid(np.arange(n), np.arange(n), indexing='xy')
    return x[bool_indexing], n-1-y[bool_indexing]


def ij_to_xy(i, j, n):
    return np.array(j), n-1-np.array(i)


def plot_matrix(n: int, x: np.ndarray, y: np.ndarray, c='tab:blue', marker='o', s=100, ax=None):
    if ax is None:
        fig, ax = plt.subplots()
    ax.scatter(x, y, s=s, c=c, marker=marker)
    ax.plot([0, -0.5-n/30, -0.5-n/30, 0], [-0.5-n/30, -0.5-n/30, n-0.5+n/30, n-0.5+n/30], c='black', linewidth=4) # [
    ax.plot([n-1, n-0.5+n/30, n+n/30-0.5, n-1], [-0.5-n/30, -0.5-n/30, n-0.5+n/30, n-0.5+n/30], c='black', linewidth=4) # ]
    ax.axis('off')
    ax.set_aspect('equal', 'box')
    return ax


class QR_simulator:
    def __init__(self, A: np.ndarray, nonzero_color='tab:blue', zero_color='black', one_color='tab:green', 
                 alpha_color='tab:orange', highlight_color='r'):
        assert A.shape[0] == A.shape[1], "QR is defined for m, n matrices, however we can only process n, n matrices."
        self.A = A
        self.n = A.shape[0]
        # current state
        self.reset()
        self.q = np.eye(self.n, dtype=np.float128)
        self.r = A.copy().astype(dtype=np.float128)
        self.qis = []
        self.rs = []
        self.ps = []
        # colors
        self.nonzero_color = nonzero_color
        self.zero_color = zero_color
        self.one_color = one_color
        self.alpha_color = alpha_color
        self.highlight_color = highlight_color

    def reset(self):
        self.q = np.eye(self.n, dtype=np.float128)
        self.r = self.A.copy().astype(dtype=np.float128)
        self.qis, self.rs, self.ps = [], [], []

    def update(self, qi, p):
        self.qis.append(qi)
        self.rs.append(self.r.copy())
        self.ps.append(p)
        self.r = qi @ self.r
        self.q = self.q @ qi.T

    def householder_reflection_step(self, j: int, hessenberg=False):
        """
        Perform the Householder reflection on a matrix mx at column j.
        if hessenberg == True, leave one sub-diagonal with non-zero values.
        """
        x = self.r[j+hessenberg:, j]
        P = np.eye(self.A.shape[0], dtype=np.float128)
        if not np.allclose(x[1:], np.zeros_like(x[1:])):
            alpha = np.linalg.norm(x)
            u1 = - np.sqrt(alpha-x[0])/np.sqrt(2*alpha)
            u = np.array([[u1] + [-xi/(2*alpha*u1) for xi in x[1:]]], dtype=np.float128).T
            P[j+hessenberg:, j+hessenberg:] = (np.eye(len(x), dtype=np.float128)-(2*u@u.T))
        qi = P
        return qi

    def givens_rotation_step(self, i: int, j: int):
        """ Do the Givens rotation on a matrix mx so that the item mx[i, j] becomes (almost) equal to zero. """
        assert j < i, "We can only do the rotation under the diagonal."
        ab = np.array([self.r[j, j], self.r[i, j]], dtype=np.float128)
        AB = ab / np.linalg.norm(ab)
        # THIS SHOULD BE NUMERICALLY MORE STABLE
        # if np.greater_equal(np.abs(ab[1]), np.abs(ab[0])):
        #     tau = ab[0]/ab[1]
        #     B = 1/np.sqrt(1+tau**2)
        #     AB = np.array([tau*B, B])
        qi = np.eye(self.A.shape[0], dtype=np.float128)
        qi[j, j] = AB[0]
        qi[i, i] = AB[0]
        qi[i, j] = AB[1]
        qi[j, i] = -AB[1]
        return qi.T

    def run_qr_decomposition(self, mode='householder') -> 'decomposition steps':
        self.reset()
        if mode == 'householder':
            for j in range(self.n):
                qi = self.householder_reflection_step(j, False)
                self.update(qi, [j])
        if mode == 'givens':
            self.ps = []
            for j in range(self.n - 1):
                for i in range(j + 1, self.n):
                    qi = self.givens_rotation_step(i, j)
                    self.update(qi, [i, j])
        return self.qis, self.rs, self.ps

    def highlight_items(self, y1, y2, x1, x2, c=None, ax=None):
        """
        Draw a red frame around items defined with y1, y2, x1, x2.
        Corresponds to the numpy selection: A[y1:y2, x1:x2]
        """
        c = self.highlight_color if c is None else c
        if ax is None:
            fig, ax = plt.subplots()
        y = self.n - np.array([y1, y1, y2, y2, y1]) - 0.5
        x = np.array([x1, x2, x2, x1, x1]) - 0.5
        ax.plot(x, y, c=c)
        return ax

    def plot_zero_nonzero(self, mx, forbidden_ij, ax, nonzero_color=None, zero_color=None, one_color=None):
        """
            Plot matrix and differentiate between zero/one/other items.
            Ignore items listed in forbidden_ij
        """
        zero_color = self.zero_color if zero_color is None else zero_color
        one_color = self.one_color if one_color is None else one_color
        nonzero_color = self.nonzero_color if nonzero_color is None else nonzero_color
        zero_mask = (np.abs(mx)-DBL_EPS) <= 0
        one_mask = (np.abs(mx-1)-DBL_EPS) <= 0
        mask = ~(zero_mask + one_mask)
        for i, j in forbidden_ij:
            mask[i, j] = False
            zero_mask[i, j] = False
            one_mask[i, j] = False
        x, y = bool_index_to_xy(mask)
        zero_x, zero_y = bool_index_to_xy(zero_mask)
        one_x, one_y = bool_index_to_xy(one_mask)
        ax = plot_matrix(self.n, x, y, c=nonzero_color, ax=ax)
        ax = plot_matrix(self.n, zero_x, zero_y, c=zero_color, marker=r"$0$", ax=ax)
        ax = plot_matrix(self.n, one_x, one_y, c=one_color, marker=r"$1$", ax=ax)
        return ax

    def plot_householder_reflection_step(self, qi, ri, j: int):
        fig, (q_ax, r_ax) = plt.subplots(1, 2, figsize=(6, 3))
        # qi = self.householder_reflection_step(j)
        # ------------------- Ri ------------------
        x_ij = [(k, j) for k in range(j, self.n)]
        forbidden_ij = x_ij if j == 0 else x_ij+[(j-1, j-1)]  # alpha from last iteration
        r_ax = self.plot_zero_nonzero(ri, forbidden_ij, r_ax)
        for ii, jj in x_ij:
            marker = r"$x_{}$".format(ii - j + 1)
            xy = ij_to_xy([ii], [jj], self.n)
            r_ax = plot_matrix(self.n, *xy, marker=marker, s=200, c=self.alpha_color, ax=r_ax)
        r_ax = self.highlight_items(j, self.n, j, j+1, ax=r_ax)
        if j != 0:
            xy = ij_to_xy([j-1], [j-1], self.n)
            r_ax = plot_matrix(self.n, *xy, marker=r"$\alpha$", c=self.alpha_color, ax=r_ax)
        r_ax.set_title(r"$R_i$", fontsize=22)
        # ------------------- q ------------------
        q_ax = self.plot_zero_nonzero(qi, [], q_ax)
        q_ax = self.highlight_items(j, self.n, j, self.n, ax=q_ax)
        q_ax.set_title(r"$Q_i$", fontsize=22)
        plt.show()

    def plot_givens_rotation_step(self, qi, ri, i: int, j: int):
        fig, (q_ax, r_ax) = plt.subplots(1, 2, figsize=(6, 3))
        # ------------------- Ri ------------------
        ab = [(j, j), (i, j)]
        r_ax = self.plot_zero_nonzero(ri, ab, r_ax)
        r_ax = plot_matrix(self.n, *ij_to_xy([j], [j], self.n), c=self.alpha_color, marker=r"$a$", ax=r_ax)
        r_ax = plot_matrix(self.n, *ij_to_xy([i], [j], self.n), c=self.alpha_color, s=150, marker=r"$b$", ax=r_ax)
        # HIGHLIGHT THE LAST ZERO
        # if i == j+1:
        #     if i > 1:
        #         r_ax = self.highlight_items(self.n-1, self.n, j-1, j, ax=r_ax)
        # else:
        #     r_ax = self.highlight_items(i-1, i, j, j+1, ax=r_ax)
        # HIGHLIGHT THE NEXT ZERO
        r_ax = self.highlight_items(i, i+1, j, j+1, ax=r_ax)
        r_ax.set_title(r"$R_i$", fontsize=22)
        # ------------------- Qi ------------------
        alphas_betas = [[j, j], [i, i], [i, j], [j, i]]
        q_ax = self.plot_zero_nonzero(qi, alphas_betas, q_ax)
        q_ax = plot_matrix(self.n, *ij_to_xy([j], [j], self.n), c=self.alpha_color, marker=r"$\alpha$", ax=q_ax)
        q_ax = plot_matrix(self.n, *ij_to_xy([i], [i], self.n), c=self.alpha_color, marker=r"$\alpha$", ax=q_ax)
        q_ax = plot_matrix(self.n, *ij_to_xy([i], [j], self.n), c=self.alpha_color, s=200, marker=r"$\beta$", ax=q_ax)
        q_ax = plot_matrix(self.n, *ij_to_xy([j], [i], self.n), c=self.alpha_color, s=200, marker=r"-$\beta$", ax=q_ax)
        q_ax.set_title(r"$Q_i^T$", fontsize=22)
        plt.show()

    def plot_step(self, i, method='householder'):
        if 0 <= i < len(self.ps):
            qi, ri, p = self.qis[i], self.rs[i], self.ps[i]
            if method == 'householder':
                self.plot_householder_reflection_step(qi, ri, *p)
            elif method == 'givens':
                self.plot_givens_rotation_step(qi, ri, *p)
            else:
                raise AttributeError(f"unknown method: {method}")
        else:
            self.plot_matrices()

    def plot_matrices(self):
        fig, (q_ax, r_ax) = plt.subplots(1, 2, figsize=(6, 3))
        # ------------------- Ri ------------------
        r_ax = self.plot_zero_nonzero(self.r, [], r_ax)
        r_ax.set_title(r"$R$", fontsize=22)
        # ------------------- Qi ------------------
        q_ax = self.plot_zero_nonzero(self.q, [], q_ax)
        q_ax.set_title(r"$Q$", fontsize=22)
        plt.show()


def main():
    n = 5
    mx = np.random.rand(n, n)
    A = mx.T @ mx  # symmetric matrix
    # A[2] = 0.
    sim = QR_simulator(A, nonzero_color='tab:blue', zero_color='black',
                       alpha_color='tab:purple', highlight_color='r')
    qis, ris, ps = sim.run_qr_decomposition('givens')
    for qi, ri, p in zip(qis, ris, ps):
        sim.plot_givens_rotation_step(qi, ri, *p)
    sim.plot_matrices()
    print(np.allclose(sim.A, sim.q@sim.r))


if __name__ == "__main__":
    main()
