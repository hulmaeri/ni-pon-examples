from functools import reduce
import numpy as np


def householder_reflection(mx: np.array, j: int, hessenberg=False):
    """
    Perform the Householder reflection on a matrix mx at column j.
    if hessenberg == True, leave one sub-diagonal with non-zero values.
    """
    assert mx.shape[0] == mx.shape[1], "We can only process n, n matrices."
    x = mx[j+hessenberg:, j]
    P = np.eye(mx.shape[0], dtype=np.float128)
    if not np.allclose(x[1:], np.zeros_like(x[1:])):
        alpha = np.linalg.norm(x)
        u1 = - np.sqrt(alpha-x[0])/np.sqrt(2*alpha)
        u = np.array([[u1] + [-xi/(2*alpha*u1) for xi in x[1:]]], dtype=np.float128).T
        P[j+hessenberg:, j+hessenberg:] = (np.eye(len(x), dtype=np.float128)-(2*u@u.T))
    return P


def givens_rotation(mx: np.array, i: int, j: int):
    """ Do the Givens rotation on a matrix mx so that the item mx[i, j] becomes (almost) equal to zero. """
    assert j < i, "We can only do the rotation under the diagonal."
    assert mx.shape[0] == mx.shape[1], "We can only work with square matrices."
    ab = np.array([mx[j, j], mx[i, j]], dtype=np.float128)
    AB = ab / np.linalg.norm(ab)
    # THIS SHOULD BE NUMERICALLY MORE STABLE
    # if np.greater_equal(np.abs(ab[1]), np.abs(ab[0])):
    #     tau = ab[0]/ab[1]
    #     B = 1/np.sqrt(1+tau**2)
    #     AB = np.array([tau*B, B])
    q = np.eye(mx.shape[0], dtype=np.float128)
    q[j, j] = AB[0]
    q[i, i] = AB[0]
    q[i, j] = AB[1]
    q[j, i] = -AB[1]
    return q.T


def almost0to0(mx: np.array):
    idx = np.isclose(np.zeros_like(mx), mx)
    mx[idx] = 0.
    return mx


def is_upper_triangular(mx: np.array):
    assert mx.shape[0] == mx.shape[1], "QR is defined for m, n matrices, however we can only process n, n matrices."
    for j in range(mx.shape[0]-1):
        if not np.allclose(mx[j+1, j], 0.):
            return False
    return True


def qr(A: np.array, method='householder'):
    """
    Do the QR decomposition.
    A = QR, where Q.TQ=E, and R is in upper triangular form.
    """
    assert A.shape[0] == A.shape[1], "QR is defined for m, n matrices, however we can only process n, n matrices."
    r = A.copy().astype(dtype=np.float128)
    q_arr = []
    for j in range(A.shape[0] - 1):
        if method == "givens":
            for i in range(j+1, A.shape[0]):
                qi = givens_rotation(r, i, j)
                r = qi@r
                q_arr.append(qi.T)
        elif method == "householder":
            qi = householder_reflection(r, j, False)
            r = qi@r
            q_arr.append(qi.T)
    q = reduce(np.matmul, q_arr)
    return q, r


def qr_hessenberg(mx: np.array):
    """ Householder reduction to Hessenberg form. """
    assert mx.shape[0] == mx.shape[1], "QR is defined for m, n matrices, however we can only process n, n matrices."
    a, r = mx.copy().astype(dtype=np.float128), mx.copy().astype(dtype=np.float128)
    q_arr = []
    for j in range(mx.shape[0] - 2):
        qi = householder_reflection(a, j, True)
        r = qi@r
        a = qi@a@qi
        q_arr.append(qi.T)
    q = reduce(np.matmul, q_arr) if q_arr else np.eye(*mx.shape)
    return q, r


def eigenvals(A: np.array):
    """
    Estimate eigenvalues of a symmetric matrix A.
    This algorithm works as follows:
        1. Do the Householder reduction to Hessenberg form.
        2. Because A is a symmetric matrix, the resulting matrix in the Hessenberg form will be also tri-diagonal.
        3. Do the QR algorithm with Givens rotation until the matrix converges to upper triangular form.
        4. Return sorted numbers on diagonal. They are eigenvalues.
    """
    if A.size == 0:
        return None
    assert is_symmetric(A), "mx is not symmetric matrix."
    q0, _ = qr_hessenberg(A.astype(np.float128))
    q = q0
    B0 = q0.T@A@q0  # B0 is tri-diagonal, B0 and A are similar
    Bi = B0.copy()
    while not is_upper_triangular(Bi):
        qi, ri = qr(Bi, 'givens')
        q = q@qi
        Bi = ri@qi
    eigenvalues = Bi.diagonal()
    idx = np.flip(np.argsort(np.abs(eigenvalues)))
    return eigenvalues[idx]


def is_symmetric(m: np.array):
    """
    Check if a matrix m is a symmetric matrix.
    :param m: matrix of shape (n, n)
    :return: True iff. m.T is close enough to m
    """
    if len(m.shape) == 2 and m.shape[0] == m.shape[1]:
        return np.allclose(m, m.T)
    return False
